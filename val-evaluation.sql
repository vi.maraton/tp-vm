-- ==================================================================
-- DML - SIMPLE - EXERCICE 1 ----------------------------------------
-- ==================================================================
-- 1. Ecrire la requête qui permet d’extraire le nom et le sexe des coureurs.

select CO_NOM Nom, REPLACE(replace(CO_SEXE, '1','homme'),'2', 'femme') Sexe from coureur;
-- 17 rows

-- ==================================================================
-- DML - SIMPLE - EXERCICE 2 ----------------------------------------
-- ==================================================================
-- 2. Ecrire la requête qui permet d’extraire la liste des clubs qui se trouvent à Bordeaux. Le
-- résultat doit être trié par ordre alphabétique des noms des clubs.

select CL_NOM Club, CL_VILLE Ville from club
where CL_VILLE ='Bordeaux'
order by CL_NOM;
-- 2 rows

-- ==================================================================
-- DML - SIMPLE - EXERCICE 3 ----------------------------------------
-- ==================================================================
-- 3. Lister les temps de Cédric Villani. 

select CO_PRENOM Prenom, CO_NOM Nom, IN_TEMP_ANNONCE 'Temps Annoncé', IN_TEMP_EFFECTUE 'Temps réalisé'
FROM coureur
INNER JOIN inscription ON coureur.CO_ID = inscription.IN_ID
where CO_PRENOM ='Cédric';
-- 1 row

-- ==================================================================
-- DML - SIMPLE - EXERCICE 4 ----------------------------------------
-- ==================================================================
-- 4. Afficher le nom, le prénom et la date de naissance des quatre hommes les plus jeunes.  

select CO_NOM Nom, CO_PRENOM Prenom, CO_NAISSANCE 'Date de naissance' from coureur
order by year(CO_NAISSANCE)
limit 4;
-- 4 rows

-- ==================================================================
-- DML - SIMPLE - EXERCICE 5 ----------------------------------------
-- ==================================================================
-- 5. Ecrire la requête qui permet d’obtenir le nom et le prénom des coureurs dont le nom 
-- contient un « o ». 

select CO_NOM Nom, CO_PRENOM Prenom
from coureur
where CO_NOM like '%o%';
-- 7 rows

-- ==================================================================
-- DML - SIMPLE - EXERCICE 6 ----------------------------------------
-- ==================================================================
--  6. Lister les femmes nées entre le 15/06/1922 et 18/10/1950.

select CO_NOM Nom, CO_PRENOM Prenom, CO_NAISSANCE 'Date de naissance'
from coureur
where CO_SEXE = 2 and CO_NAISSANCE between '1922-06-15' and '1950-10-18';
-- 4 rows

-- ==================================================================
-- DML - SIMPLE - EXERCICE 7 ----------------------------------------
-- ==================================================================
--  7. Afficher la liste des villes dans lesquelles se trouvent des clubs. 

select distinct CL_VILLE 'Ville avec club' from club
where CL_NOM is not null;
-- 7 rows

-- ==================================================================
-- DML - SIMPLE - EXERCICE 8 ----------------------------------------
-- ==================================================================
--  8. Ecrire la requête qui permet de lister les heures de départ des épreuves qui se sont 
-- déroulées en avril 2016. 

select EP_HEURE 'Heure de départ', EP_DATE 'Date de l\'épreuve' from epreuve
where year(EP_DATE) = '2016' and month(EP_DATE) = '04';
-- 3 rows

-- ==================================================================
-- DML - SIMPLE - EXERCICE 9 ----------------------------------------
-- ==================================================================
-- 9. Ecrire la requête qui fournit la catégorie d’un coureur âgé de 21 ans. 

select CA_LIBELLE Catégorie from categorie_age
where CA_AGEDEB and CA_AGEFIN between 20 and 22;
-- 1 row

-- ==================================================================
-- DML - SIMPLE - EXERCICE 10 ---------------------------------------
-- ==================================================================
-- 10. Ecrire la requête qui permet de lister les coureurs dont le prénom commence par la lettre 
-- « S » ou « B » et dont le nom contient un « w ». Il faut extraire les noms et les dates de 
-- naissance.

select CO_NOM Nom, CO_PRENOM Prenom, CO_NAISSANCE 'Date de naissance' from coureur
where 
CO_NOM like '%w%'
and CO_PRENOM like 'S%' or 'B%';
-- 1 row

-- ==================================================================
-- DML - AVANCE - EXERCICE 1 ----------------------------------------
-- ==================================================================
-- 1. Afficher le jour et le mois des épreuves de 2016. Le mois doit être écrit en toutes lettres 
-- et le résultat doit s’afficher dans une seule colonne.

select concat(day(EP_DATE)," ", monthname(EP_DATE)," ", year(EP_DATE)) 'jour et mois' from epreuve
where year(EP_DATE) = 2016;
-- 5 rows

-- ==================================================================
-- DML - AVANCE - EXERCICE 2 ----------------------------------------
-- ==================================================================
-- 2. Lister les coureurs masculins en affichant leur prénom et nom dans une colonne et leur âge

select concat(CO_PRENOM," ", CO_NOM) 'Prénom et nom', IN_AGE Age from coureur
join inscription on coureur.CO_ID = inscription.IN_ID
where CO_SEXE = '1';
-- 10 rows

-- ==================================================================
-- DML - AVANCE - EXERCICE 3 ----------------------------------------
-- ==================================================================
-- 3. Ecrire la requête qui permet de lister le nom et la date des épreuves de 2016 dont le 
-- départ s’est déroulé avant 14h. Afficher la date dans un format lisible. 

select concat(TY_LIBELLE," ","Le"," ", day(EP_DATE)," ", monthname(EP_DATE)," ", year(EP_DATE)," ","à"," ", hour(EP_HEURE),'h') 'jour et mois'  from epreuve
join type_epreuve on epreuve.EP_ID = type_epreuve.TY_ID
where year(EP_DATE) = 2016 and hour(EP_HEURE) < '14';
-- 1 row

-- ==================================================================
-- DML - AVANCE - EXERCICE 4 ----------------------------------------
-- ==================================================================
-- 4. Lister les coureurs nés entre 1930 et 1960. Cette liste doit être triée du plus vieux au plus 
-- jeune, et le sexe doit être spécifié en toutes lettres.

select concat(CO_NOM," ", CO_PRENOM) 'Nom et prenom', REPLACE(replace(CO_SEXE, '1','homme'),'2', 'femme') Sexe, CO_NAISSANCE 'Date de naissance' from coureur
where year(CO_NAISSANCE) between 1930 and 1960
order by  CO_NAISSANCE desc;
-- 8 rows

-- ==================================================================
-- DML - AVANCE - EXERCICE 5 ----------------------------------------
-- ==================================================================
-- 5. Ecrire la requête qui liste les clubs dont le nom ne commence pas par le mot « les ». 

select CL_NOM 'Nom du club' from club
where CL_NOM not like 'les%';
-- 3 rows

-- ==================================================================
-- DML - AVANCE - EXERCICE 6 ----------------------------------------
-- ==================================================================
-- 6. Lister les coureurs nés en janvier ou en avril. Le prénom et le nom doivent être dans une 
-- seule colonne. 

select concat(CO_PRENOM," ",CO_NOM) 'Nom et prenom', CO_NAISSANCE 'Date de naissance' from coureur
where month(CO_NAISSANCE) = 01 or month(CO_NAISSANCE) = 04;
-- 4 rows

-- ==================================================================
-- DML - AVANCE - EXERCICE 8 ----------------------------------------
-- ==================================================================
-- 8. Fournir la liste des catégories d’âge dont le nom commence par les lettres « M » ou « B ».

select CA_LIBELLE 'Categorie d\'âge' from categorie_age
where CA_LIBELLE like 'M%' or CA_LIBELLE like 'B%';

-- ==================================================================
-- DML - AVANCE - EXERCICE 9 ----------------------------------------
-- ==================================================================
-- 9. Lister les quinze premiers caractères du nom des clubs de Bordeaux.

select SUBSTRING(CL_NOM, 1, 15) 'Nom du club', CL_VILLE Ville from club
where CL_VILLE = 'Bordeaux';
-- 2 rows

-- ==================================================================
-- DML - AVANCE - EXERCICE 10 ---------------------------------------
-- ==================================================================
-- 10. Lister les coureurs en remplaçant les lettres « é » dans le prénom par « e ».

select concat(replace(CO_PRENOM,'é', 'e')," ",CO_NOM) 'Nom et prenom' from coureur;
-- 17 rows

-- ==================================================================
-- DML - JOINTURES - EXERCICE 1 -------------------------------------
-- ==================================================================
-- 1. Lister les manifestations en affichant le nom du championnat auquel elles appartiennent 
-- ainsi que la date de début.

select MA_NOM 'Nom manifestation', CH_NOM 'Nom championnat', CH_DATEDEB 'Date de début' from manifestation
left join championnat on manifestation.MA_ID = championnat.CH_ID;
-- 3 rows

-- ==================================================================
-- DML - JOINTURES - EXERCICE 2 -------------------------------------
-- ==================================================================
-- 2. Ecrire la requête qui fournit la liste des femmes ainsi que le nom du club auquel elles 
-- ont adhéré en 2015.

select concat(CO_NOM," ", CO_PRENOM) 'Nom et prenom', CL_NOM 'Nom du club' from coureur
join adhesion on coureur.CO_ID = adhesion.AD_COUREUR_FK
join club on adhesion.AD_CLUB_FK = club.CL_ID
where CO_SEXE = '2' and  AD_ANNEE = '2015';
-- 3 rows

-- ==================================================================
-- DML - JOINTURES - EXERCICE 3 -------------------------------------
-- ==================================================================
-- 3. Afficher les temps réalisés des participants au semi-marathon du marathon de Bordeaux 
-- en 2016.

select IN_TEMP_EFFECTUE 'Temps effectué', TY_LIBELLE "Type d'épreuve", MA_NOM "Nom de l'épreuve" from inscription
join epreuve on inscription.IN_EPREUVE_FK = epreuve.EP_ID
join type_epreuve on epreuve.EP_TYPE_FK = type_epreuve.TY_ID
join manifestation on epreuve.EP_MANIF_FK = manifestation.MA_ID
where TY_LIBELLE = 'Semi marathon' and MA_NOM = 'Marathon de Bordeaux';
-- 7 rows

-- ==================================================================
-- DML - JOINTURES - EXERCICE 4 -------------------------------------
-- ==================================================================
-- 4. Lister les coureurs adhérents à la foulée arcachonnaise qui ont déjà participé au 
-- marathon de Bordeaux.

select distinct concat(CO_NOM," ", CO_PRENOM) 'Nom et prenom', CL_NOM 'Nom du club', MA_NOM 'Nom manifestation' from coureur
join adhesion on coureur.CO_ID = adhesion.AD_COUREUR_FK
join club on adhesion.AD_CLUB_FK = club.CL_ID
join inscription on coureur.CO_ID = inscription.IN_COUREUR_FK
join epreuve on inscription.IN_EPREUVE_FK = epreuve.EP_ID
join manifestation on epreuve.EP_MANIF_FK = manifestation.MA_ID
where CL_NOM = 'La foulée bordelaise' and MA_NOM = 'Marathon de Bordeaux';
-- 4 rows

-- ==================================================================
-- DML - JOINTURES - EXERCICE 5 -------------------------------------
-- ==================================================================
-- 5. Afficher les coureurs qui n’ont jamais adhéré à un club.

select distinct concat(CO_NOM," ", CO_PRENOM) 'Nom et prenom', CL_NOM 'Nom du club' from coureur
left join adhesion on coureur.CO_ID = adhesion.AD_COUREUR_FK
left join club on adhesion.AD_CLUB_FK = club.CL_ID
where CL_NOM is null;
-- 2 rows

-- ==================================================================
-- DML - JOINTURES - EXERCICE 6 -------------------------------------
-- ==================================================================
-- 6. Qui sont les deux coureurs ayant fait les meilleurs temps lors du marathon du Médoc en 2016.

select concat(CO_NOM," ", CO_PRENOM) 'Nom et prenom' from coureur
join inscription on coureur.CO_ID = inscription.IN_COUREUR_FK
join epreuve on inscription.IN_EPREUVE_FK = epreuve.EP_ID
join manifestation on epreuve.EP_MANIF_FK = manifestation.MA_ID
where MA_NOM = 'Marathon du Médoc' and  year(EP_DATE) = 2016
order by IN_TEMP_EFFECTUE desc
limit 2;
-- 1 row

-- ==================================================================
-- DML - JOINTURES - EXERCICE 7 -------------------------------------
-- ==================================================================
 -- 7. Lister les femmes ayant déjà été inscrites à une épreuve dans la catégorie Masters.
 
 select concat(CO_NOM," ", CO_PRENOM) 'Nom et prenom' from coureur
 left join inscription on coureur.CO_ID = inscription.IN_COUREUR_FK
 join categorie_age on inscription.IN_CATEG_AGE_FK = categorie_age.CA_ID
 where CO_SEXE = '2' and CA_LIBELLE = 'Masters';
 -- 2 rows
 
 -- =================================================================
-- DML - JOINTURES - EXERCICE 8 -------------------------------------
-- ==================================================================
-- 8. Afficher la liste des coureurs n’ayant participé à aucune épreuve.

select concat(CO_NOM," ", CO_PRENOM) 'Nom et prenom', EP_ID 'ID de l\épreuve' from coureur
left join inscription on coureur.CO_ID = inscription.IN_COUREUR_FK
left join epreuve on inscription.IN_EPREUVE_FK = epreuve.EP_ID
where EP_ID is null;
-- 2 rows

-- ==================================================================
-- DML - JOINTURES - EXERCICE 9 -------------------------------------
-- ==================================================================
-- 9. Afficher la liste des coureurs, triés par ordre alphabétique, adhérents du club « les 
-- scientifiques font du sport » en 2016.

select concat(CO_NOM," ", CO_PRENOM) 'Nom et prenom', CL_NOM 'Nom du club', AD_ANNEE Date from coureur
join adhesion on coureur.CO_ID = adhesion.AD_COUREUR_FK
join club on adhesion.AD_CLUB_FK = club.CL_ID
where AD_ANNEE = '2016' and CL_NOM = 'Les scientifiques font du sport'
order by CO_NOM;
-- 1 row

-- ==================================================================
-- DML - JOINTURES - EXERCICE 10 ------------------------------------
-- ==================================================================
-- 10. Quels sont les coureurs ayant déjà parcouru un marathon en plus de 3 heures ? Préciser 
-- le prénom-nom des coureurs, le nom du marathon, le temps et l’année.

select concat(CO_NOM," ", CO_PRENOM) 'Nom et prenom', MA_NOM 'Nom du marathon', IN_TEMP_EFFECTUE 'Temp effectué', year(EP_DATE) 'Date' from coureur
join inscription on coureur.CO_ID = inscription.IN_COUREUR_FK
join epreuve on inscription.IN_EPREUVE_FK = epreuve.EP_ID
join manifestation on epreuve.EP_MANIF_FK = manifestation.MA_ID
where hour(IN_TEMP_EFFECTUE) >= '3';
-- 7 rows

-- ==================================================================
-- DML – AGREGATION - EXERCICE 1 ------------------------------------
-- ==================================================================
-- 1. Afficher le nombre de coureurs inscrits au marathon de Bordeaux 2015, regroupés en club.

select count(AD_COUREUR_FK) 'Nombre de coureurs' from coureur
join inscription on coureur.CO_ID = inscription.IN_COUREUR_FK
join epreuve on inscription.IN_EPREUVE_FK = epreuve.EP_ID
join manifestation on epreuve.EP_MANIF_FK = manifestation.MA_ID
join adhesion on coureur.CO_ID = adhesion.AD_COUREUR_FK
join club on adhesion.AD_CLUB_FK = club.CL_ID
where MA_NOM = 'Marathon de Bordeaux' and year(EP_DATE) = '2015'
group by CL_NOM;
-- 0 row

-- ==================================================================
-- DML – AGREGATION - EXERCICE 2 ------------------------------------
-- ==================================================================
-- 2. Quel est le nombre de coureurs inscrits au marathon de Bordeaux 2015, regroupés par 
-- club, mais uniquement pour les clubs bordelais ?

-- ***************** NON TERMINE

-- ==================================================================
-- DML – AGREGATION - EXERCICE 3 ------------------------------------
-- ==================================================================
-- 3. Quel est le temps moyen de chacune des épreuves des marathons présents dans la base ?

select SEC_TO_TIME (avg(time_to_sec(IN_TEMP_EFFECTUE))) 'Temps moyen par épreuve', TY_LIBELLE from inscription
join epreuve on inscription.IN_EPREUVE_FK = epreuve.EP_ID
join type_epreuve on epreuve.EP_TYPE_FK = type_epreuve.TY_ID
group by TY_DISTANCE;
-- 3 rows

-- ==================================================================
-- DML – AGREGATION - EXERCICE 4 ------------------------------------
-- ==================================================================
-- 4. Quel est le temps et le nom du gagnant de chaque épreuve du marathon de Bordeaux 
-- édition 2015 ?

-- ***************** NON TERMINE

-- ==================================================================
-- DDL - EXERCICE 1 -------------------------------------------------
-- ==================================================================
-- 1. Une erreur de saisie sur la date de naissance d’Aldrin Buzz a été faite lors de son insertion 
-- dans la base. Ecrire la requête qui permet de la modifier. Il est né le 20 janvier 1948.

UPDATE coureur
SET CO_NAISSANCE = REPLACE(CO_NAISSANCE, '1930-01-20', '1948-01-20')
where CO_ID = '1';

select CO_NOM Nom, CO_PRENOM Prenom, CO_NAISSANCE 'Date de naissance' from coureur
where CO_NOM = 'Aldrin';

-- ==================================================================
-- DDL - EXERCICE 2 -------------------------------------------------
-- ==================================================================
-- 2. Ecrire les requêtes pour créer le marathon de La Rochelle. Le marathon et 
-- le semi-marathon se déroulent tous les deux le 27 novembre 2016 et débutent à 9H.

INSERT INTO `MANIFESTATION` (`MA_ID`, `MA_NOM`,`MA_CHAMP_FK`) VALUES
(5, 'Marathon de la Rochelle','1');
INSERT INTO `EPREUVE` (`EP_ID`, `EP_DATE`, `EP_HEURE`, `EP_TYPE_FK`, `EP_MANIF_FK`, `EP_NB`) VALUES
(5, '2016-11-27', '09:00:00', 1, 2, 2);

select MA_NOM 'Nom manifestation', EP_DATE 'Date', EP_HEURE Heure from manifestation
join epreuve on manifestation.MA_ID = epreuve.EP_ID
join type_epreuve on epreuve.EP_TYPE_FK = type_epreuve.TY_ID;

-- ==================================================================
-- DDL - EXERCICE 3 -------------------------------------------------
-- ==================================================================
-- 3. Insérer un nouveau coureur : Benoit Mandelbrot né le 20 novembre 1924.

INSERT INTO `COUREUR` (`CO_ID`, `CO_NOM`, `CO_PRENOM`, `CO_NAISSANCE`, `CO_SEXE`) VALUES
(18, 'Mandelbrot', 'Benoit', '1924-11-20', 1);

Select CO_NOM Nom, CO_PRENOM Prenom, CO_NAISSANCE 'Date de naissance' from coureur;

-- ==================================================================
-- DDL - EXERCICE 4 -------------------------------------------------
-- ==================================================================
-- 4. Ecrire la requête qui permet de diminuer d’une minute les temps réalisés de tous les 
-- coureurs du marathon de Paris de 2015, sur toutes les épreuves.

-- ***************** NON TERMINE
UPDATE inscription
SET IN_TEMP_EFFECTUE = minute(IN_TEMP_EFFECTUE) - minute(1)
where IN_ID is not null;

-- ==================================================================
-- DDL - EXERCICE 5 -------------------------------------------------
-- ==================================================================
-- 5. Supprimer toutes les inscriptions au marathon du Médoc édition 2016.

-- ***************** NON TERMINE

-- ==================================================================
-- DDL - EXERCICE 1 -------------------------------------------------
-- ==================================================================
-- 1. Ecrire la requête qui permet d’écrire la table INSCRIPTION.

	CREATE TABLE IF NOT EXISTS `INSCRIPTION` (
	`IN_ID` int(11) NOT NULL AUTO_INCREMENT
	);

-- ==================================================================
-- DDL - EXERCICE 2 -------------------------------------------------
-- ==================================================================
-- 2. Ecrire la requête qui permet de créer la clé primaire de la table « coureur ».

CREATE TABLE IF NOT EXISTS `COUREUR` (
  `CO_ID` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`CO_ID`)
);

-- ==================================================================
-- DDL - EXERCICE 3 -------------------------------------------------
-- ==================================================================
-- 3. Modifier le type de données du champ AD_Licence de la table ADEHSION en 
-- INTEGER.

ALTER TABLE adhesion
MODIFY AD_LICENCE integer;

-- ==================================================================
-- DDL - EXERCICE 4 -------------------------------------------------
-- ==================================================================
-- 4. Créer une nouvelle table regroupant les noms, prénoms et dates de naissance des 
-- coureurs féminins. Cette table sera nommée COUREUR_FEMME et contiendra des 
-- données déjà existante dans la base.

create table COUREUR_FEMME as 
select CO_NOM Nom, CO_PRENOM Prenom, CO_NAISSANCE 'Date de naissance' from coureur
where CO_SEXE = 2;

-- ==================================================================
-- DDL - EXERCICE 5 -------------------------------------------------
-- ==================================================================
-- 5. Ecrire les requêtes qui permettent de créer les différentes clés de la table EPREUVE.

CREATE TABLE IF NOT EXISTS `EPREUVE` (
  `EP_ID` int(11) NOT NULL AUTO_INCREMENT,
  `EP_TYPE_FK` int(11) NOT NULL,
  `EP_MANIF_FK` int(11) NOT NULL,
  PRIMARY KEY (`EP_ID`),
  KEY `FK_EPREUVE_TY_ID_idx` (`EP_TYPE_FK`),
  KEY `FK_EPREUVE_MA_ID_idx` (`EP_MANIF_FK`)
);

-- ==================================================================
-- DDL - EXERCICE 6 -------------------------------------------------
-- ==================================================================
-- 6. Ecrire les requêtes qui permettent de créer la table CATEGORIE_AGE en spécifiant une 
-- contrainte qui vérifie que la date de début est inférieure à la date de fin.

CREATE TABLE IF NOT EXISTS `CATEGORIE_AGE` (
  `CA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CA_LIBELLE` text NOT NULL,
  `CA_AGEDEB` int(11) NOT NULL,
  `CA_AGEFIN` int(11) NOT NULL,
  PRIMARY KEY (`CA_ID`),
  CHECK (CA_AGEDEB < CA_AGEFIN)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- ==================================================================
-- DDL - EXERCICE 7 -------------------------------------------------
-- ==================================================================
-- 7. Modifier la table INSCRIPTION en renommant le champ
-- IN_CERTIF en IN_CERTIF_MEDICAL.

ALTER TABLE inscription CHANGE COLUMN IN_CERTIF IN_CERTIF_MEDICAL tinyint;

-- ==================================================================
-- DDL - EXERCICE 8 -------------------------------------------------
-- ==================================================================
-- 8. Ecrire la requête qui permet de renommer la table COUREUR en participant.

ALTER TABLE coureur rename participant;

-- ==================================================================
-- DDL - EXERCICE 9 -------------------------------------------------
-- ==================================================================
-- 9. Ajouter un champ contenant le nom du département dans la table CLUB.

ALTER TABLE club 
	add column `CL_NOM_DEPARTEMENT` varchar(15);
    
-- ==================================================================
-- DDL - EXERCICE 10 ------------------------------------------------
-- ==================================================================
-- 10. Ecrire la requête qui permet de modifier le type de données du champ contenant les 
-- cotisations des adhérents des clubs, afin qu’il devienne un nombre à deux décimales.

-- ***************** NON TERMINE
ALTER TABLE adhesion
MODIFY AD_COTISATION integer;
UPDATE adhesion
set AD_COTISATION = round(AD_COTISATION, -1)
WHERE AD_COTISATION is not null;


    
    
































